//
//  CollectionViewCell.swift
//  TataCliq
//
//  Created by Dhanvanthi P on 28/06/18.
//  Copyright © 2018 Dhanvanthi P. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
   
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
